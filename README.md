# Spring Boot JPA MySQL - Building Rest CRUD API example

## Overview of Spring Boot JPA Rest CRUD API example
We will build a Spring Boot JPA Rest CRUD API for a Tutorial application in that:

This repository is Backend for [VueJS Client App](https://gitlab.com/nodejs-vuejs/vuejs-client-crud)

Each Tutorial has id, title, description, published status.
APIs help to create, retrieve, update, delete Tutorials.
Apis also support custom finder methods such as find by published status or by title.
These are APIs that we need to provide:

|Methods|Urls	        | Actions:
| ---   | ---           | ---               |
|POST	|/api/tutorials	|create new Tutorial|
|GET	|/api/tutorials	|retrieve all Tutorials|
|GET	|/api/tutorials/:id	|retrieve a Tutorial by :id|
|PUT	|/api/tutorials/:id	|update a Tutorial by :id|
|DELETE	|/api/tutorials/:id	|delete a Tutorial by :id|
|DELETE	|/api/tutorials	|delete all Tutorials|
|GET	|/api/tutorials/|published	find all published Tutorials|
|GET	|/api/tutorials?title=[keyword]	|find all Tutorials which title contains keyword|

* We make CRUD operations & finder methods with Spring Data JPA’s JpaRepository.
* The database could be PostgreSQL or MySQL depending on the way we configure project dependency & datasource.

## Things to do list:
1. Clone this repository: `git clone https://gitlab.com/spring-boot-vuejs-example/springboot-rest-tutorial.git`.
2. Go inside the folder: `cd springboot-rest-tutorial`.

## Run & Test Spring Boot application
Run Spring Boot application with command:
```
mvn clean spring-boot:run
```

tutorials database & table will be automatically generated in Database.
If you check MySQL for example, you can see things like this:
```shell script
MariaDB [tutorial]> describe tutorials;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| description | varchar(255) | YES  |     | NULL    |                |
| published   | bit(1)       | YES  |     | NULL    |                |
| title       | varchar(255) | YES  |     | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
4 rows in set (0.003 sec)
```

#### Create some Tutorials:

Add New Tutorial

![Add New Tutorial](img/add.png "Add New Tutorial")

```shell script
MariaDB [tutorial]> select * from tutorials;
+----+------------------------------+-----------+-------------------------+
| id | description                  | published | title                   |
+----+------------------------------+-----------+-------------------------+
|  1 | Descripction for Tutorial #1 |           | Spring Boot Tutorial #1 |
|  2 | Description for Tutorial #2  |           | Spring Boot Tutorial #2 |
|  3 | Description for Tutorial #3  |           | Spring Boot Tutorial #3 |
|  4 | Description for Tutorial #4  |           | Spring Boot Tutorial #4 |
|  5 | Description for Tutorial #5  |           | Spring Boot Tutorial #5 |
+----+------------------------------+-----------+-------------------------+
5 rows in set (0.000 sec)
```
#### Update some Tutorials:

Update some Tutorials

![Update some Tutorials](img/update.png "Update some Tutorials")

![Update some Tutorials](img/update2.png "Update some Tutorials")

#### Get all Tutorials:

![Get all Tutorials](img/list.png "Get all Tutorials")

#### Get a Tutorial by Id:

Get a Tutorial by Id

![Get a Tutorial by Id](img/find.png "Get a Tutorial by Id")

#### Find all published Tutorials:

![Find all published Tutorials](img/published.png "Find all published Tutorials")

#### Find all Tutorials which title contains ‘boot’:

![Get a Tutorial by Id](img/find2.png "Get a Tutorial by Id")

#### Delete a Tutorial:

Delete a Tutorial

![Delete a Tutorial](img/delete1.png "Delete a Tutorial")

```shell script
MariaDB [tutorial]> select * from tutorials;
+----+------------------------------+-----------+-------------------------+
| id | description                  | published | title                   |
+----+------------------------------+-----------+-------------------------+
|  1 | Descripction for Tutorial #1 |          | Spring Boot Tutorial #1 |
|  2 | Description for Tutorial #2  |           | Spring Boot Tutorial #2 |
|  3 | Description for Tutorial #3  |           | Spring Boot Tutorial #3 |
|  4 | Description for Tutorial #4  |           | Spring Boot Tutorial #4 |
+----+------------------------------+-----------+-------------------------+
4 rows in set (0.000 sec)

```

#### Delete all Tutorials:

Delete all Tutorials

![Delete all Tutorials](img/deleteAll.png "Delete all Tutorials")

```shell script
MariaDB [tutorial]> select * from tutorials;
Empty set (0.000 sec)


```

 



